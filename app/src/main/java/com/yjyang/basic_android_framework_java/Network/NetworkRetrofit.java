package com.yjyang.basic_android_framework_java.Network;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yao-Jung on 2018/1/27.
 */

public class NetworkRetrofit {
    public static final String BASE_URL = "";

    private volatile static NetworkRetrofit instance;
    private Retrofit retrofit;
    private static final Object lock = new Object();

    private NetworkRetrofit(){
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new NetworkRequestInterceptor())
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    public static NetworkRetrofit getInstance(){
        if(instance == null){
            synchronized (lock){
                instance = new NetworkRetrofit();
            }
        }
        return instance;
    }

    private static class NetworkRequestInterceptor implements Interceptor{

        private static final String API_KEY = "d0a59fc530b2895c618b815b862b888f";
        private static final String API_PARAMETER_NAME = "api_key";
        private static final String RESPONSE_FORMAT_PARAMETER_NAME = "format";
        private String credentials;
        private static String JSON_RESPONSE_FORMAT = "json";

        NetworkRequestInterceptor(String username, String password){
            credentials = Credentials.basic(username, password);
        }

        NetworkRequestInterceptor(){}

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();
            HttpUrl originalUrl = original.url();

            HttpUrl interceptedUrl = originalUrl.newBuilder()
                    .addQueryParameter(API_PARAMETER_NAME, API_KEY)
                    .addQueryParameter(RESPONSE_FORMAT_PARAMETER_NAME, JSON_RESPONSE_FORMAT)
                    .build();

            Request.Builder requestBuilder = original.newBuilder()
                    .url(interceptedUrl);
            Request request = requestBuilder.build();
            return chain.proceed(request);
        }
    }

}
